UART
====
uart is disabled by defualt. To enable it either start with a graphich screen and run:
```
sudo raspi-config
=> interface options => serial port => select yes
```
or you can add the line `enable_uart=1` to the file /boot/config.txt
(you can reach the file other from the graphics of rpi or you can mount the boot partition)

Starting minicom with 'hardware flow control' disabled:

we can do it by creating a file /etc/minicom/minirc.dfl:
```
sudo minicom -s
```
Then: ctrl+A Z, -> cOnfigure -> serial port -> set 'hardware flow contorl to NO' -> go back to previous menu and choose 'save as dlf'
Then this will create a file with this config in `/etc/minicom/minirc.dfl`

The picocom command is:
```
sudo picocom -b 115200 /dev/ttyUSB0
```

At this point I have:
```
root@raspberrypi:~# uname -a
Linux raspberrypi 5.7.7-v7+ #1 SMP Sun Jul 12 09:45:00 CEST 2020 armv7l GNU/Linux
root@raspberrypi:~# dmesg | grep imx219
[    8.864687] imx219 10-0010: Link frequency not supported: 297000000
[    8.864721] imx219: probe of 10-0010 failed with error -22
[   33.755873] imx219_vddl: disabling
[   33.755882] imx219_vdig: disabling
[   33.755888] imx219_vana: disabling
```

login:

pi:raspberry
root:root

Mounting:
=========
Download from: https://www.raspberrypi.org/software/operating-systems/#raspberry-pi-desktop

There are the instructions here,
https://raspberrypi.stackexchange.com/questions/13137/how-can-i-mount-a-raspberry-pi-linux-distro-image
but actually, the ubuntu creates them automatically under /media/dafna

NOTE that sometimes the `rootfs` is mounted as `rootfs1`!!
Then accordinginly I have:

dd
==
the instructions https://www.raspberrypi.org/documentation/installation/installing-images/linux.md
```
dd bs=4M if=2020-12-02-raspios-buster-armhf-full.img of=/dev/sda status=progress conv=fsync
```
**NOTE!!**, at some point it seems that the `dd` is stuck but it is not and it is important to wait till it ends!!
rpi3.sh
-------
```
export O=/home/dafna/git/3a-project/kbuild/rpi3-b-arm7
export KBUILD_OUTPUT=/home/dafna/git/3a-project/kbuild/rpi3-b-arm7
# export CROSS_COMPILE=aarch64-linux-gnu-
export CROSS_COMPILE=arm-linux-gnueabihf-
export ARCH=arm
export KERNEL=kernel7
export SOURCE=/home/dafna/git/3a-project/linux
```

rpi3-install.sh
---------------
```
source ./rpi3.sh

echo $ARCH
echo $O
echo $CROSS_COMPILE
echo $KBUILD_OUTPUT

#make -j9
cd $KBUILD_OUTPUT  && sudo make INSTALL_MOD_PATH=/media/dafna/rootfs modules_install && cd -

cp $KBUILD_OUTPUT/arch/$ARCH/boot/zImage /media/dafna/boot/$KERNEL.img
# cp $KBUILD_OUTPUT/arch/$ARCH/boot/zImage /media/dafna/boot/kernel8.img
cp $KBUILD_OUTPUT/arch/$ARCH/boot/dts/*.dtb /media/dafna/boot/
cp $KBUILD_OUTPUT/arch/$ARCH/boot/dts/overlays/*.dtb* /media/dafna/boot/overlays
cp $SOURCE/arch/$ARCH/boot/dts/overlays/README /media/dafna/boot/overlays

# umount /media/dafna/rootfs
# umount /media/dafna/boot
```
enable ssh to root:

```
root@raspberrypi:~# sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
systemctl enable ssh
systemctl start ssh
systemctl status ssh # see that it runs
```
See https://phabricator.collabora.com/T20669 for sshfs

```
sudo sshfs -o nonempty root@10.42.0.53:/lib/modules /home/dafna/git/3a-project/sshfs-mount/lib/modules/
sudo make INSTALL_MOD_PATH=/home/dafna/git/3a-project/sshfs-mount modules_install
```
to unmount the fuse ssh:
```
sudo fusermount -u /home/dafna/git/3a-project/sshfs-mount/lib/modules
```

```
now I have:
```
root@raspberrypi:~# lsmod | grep imx219
imx219                 20480  1
v4l2_fwnode            20480  2 bcm2835_unicam,imx219
videodev              237568  9 bcm2835_unicam,imx219,bcm2835_isp,v4l2_fwnode,bcm2835_codec,videobuf2_common,bcm2835_v4l2,v4l2_mem2mem,videobuf2_v4l2
mc                     40960  8 bcm2835_unicam,imx219,bcm2835_isp,bcm2835_codec,videobuf2_common,videodev,v4l2_mem2mem,videobuf2_v4l2
```
root@raspberrypi:~# dmesg | grep bcm2835_isp
[    7.458534] bcm2835_isp: module is from the staging directory, the quality is unknown, you have been warned.
[    7.470389] bcm2835-isp bcm2835-isp: bcm2835_isp_probe: ril.isp returned 1 i/p (1 expected), 2 o/p (3 expected) ports
```
```
root@raspberrypi:~# dmesg | grep imx219
[   33.755976] imx219_vddl: disabling
[   33.755992] imx219_vdig: disabling
[   33.756005] imx219_vana: disabling

7 Aug 2020
----------

```
root@raspberrypi:~# uname -a
Linux raspberrypi 5.4.50-v7+ #2 SMP Sun Aug 2 23:13:06 CEST 2020 armv7l GNU/Linux
```
linux has 5.4
rpi-download-linux 5.8
rpi-linux 4.19

```

RPI Camera Algorithms and Tuning Guide
======================================
======================================

GAIN:
-----
https://en.ids-imaging.com/tl_files/downloads/techtip/TechTip_Gain_EN.pdf
https://www.edmundoptics.com/knowledge-center/application-notes/imaging/basics-of-digital-camera-settings-for-improved-imaging-results/
https://www.dpreview.com/forums/post/62353361

The CamHelper Class
-------------------

readlist: SMIA: http://read.pudn.com/downloads95/doc/project/382834/SMIA/SMIA_Introduction_and_overview_1.0.pdf

cam_helper_ov5647.cpp
cam_helper_imx219.cpp
cam_helper_imx477.cpp

export "LIBCAMERA_LOG_LEVELS=*:DEBUG"


17 Sep:
-------
The isp driver seems to be missing, we might want this patch: https://github.com/raspberrypi/linux/pull/3715/commits/3da092fc6aff46f8ab05c7091bae4ea33bd8fa07

23 Sep:
-------
dafna@guri:~/git/3a-project$ scp root@10.42.0.53:/root/*.dot .
root@10.42.0.53's password:
media0.dot                                                                                        100%  432   484.1KB/s   00:00
media1.dot                                                                                        100%  656   691.4KB/s   00:00
rpi3.dot                                                                                          100% 1149   606.0KB/s   00:00

to make the kernel 5.4 work with the isp driver I had to also udpate the firmware with the `rpi-update` command.
Otherwise I get the error:
```
bcm2835-isp bcm2835-isp: bcm2835_isp_probe: ril.isp returned 1 i/p (1 expected), 2 o/p (3 expected) ports
```
when isp probe.

26 oct:
-------
on the camera:

```
cam -l
cam -c 1 -C -F -s width=800,height=600
cat frame-stream0-* > /root/video-800x600-bgra.bin
```
on the laptop:
```
scp root@10.42.0.53:/root/video* .
ffplay -f rawvideo -pixel_format bgra -video_size 800x600 video-800x600-bgra.bin

27 oct:
-------
If I get some issues in probe of some drivers, it might be that I only updated the kernel
without runnung the `rpi-update` that also updates the firmware.
See: https://github.com/raspberrypi/linux/pull/3715#issuecomment-694981959
Noted also that v5.9 is not usable for rpi since the isp driver `bcm2835-v4l2-isp.c` is missing.

I am able to set nice pictures from the rpi with kernel: `Linux raspberrypi 5.4.65-v7+`
After a while it crashes with NULL pointer dereference.
trying rpi-update fails with:
```
curl: (56) OpenSSL SSL_read: error:1408F119:SSL routines:ssl3_get_record:decryption failed or bad record mac, errno 0
```
I also noted that the isp driver is missing from the v5.9 release:

Today I was in a nice talk in the ELCE 2020 of Eugen Hristev
Few notes from his talk:
1. White Balance Algorithm: The white balance algorithm is always done on bayer formats. In bayer formats we have 4 channels:
R (red) , B (blue), and then we two different types of green which are GR and GB.
GR is the green pixels in raws of the green-red and GB are the green pixels of the green-blue raws.

what is the RG channel? is it pairs of R+G adjusted components? so if each pixel is say 8 bits, the RG is 16 bits?
Answer: No. Each Bayer cell has 4 cells actually. The Red, the Blue, and two green cells, which I will call: Green Red (the green on the same line with Red cell), and the Green blue (the green on the line with blue). The bayer array is formed of many of these 4color cells, which repeat themselves. We need to compute histogram for each of these 4 channels. So, the RG channel is one of the two green channels, the one on the same line with the Red pixel

2. The white balance algorithm is based on the 'grey world assumption'. If we take a typical image and we calculate
the average of all pixels we get grey. So for example if we take a photo of the color checker that is very colorful
and we take the avarge from it we get grey.
Under this assumption we look at the histogram of each of the 4 channels. and we do something that is similar to
historgram equalization for each channel separately.

so the white balance is something similar to histogram equalization?
Answer: yes ! but its more than that. if your scenery is not GREY in average, histogram equalization would mean that you turn a red(for example) scene into grey. so you have to equalize the histogram under the grey world assumption. that's why we use the color checker card

3. Exposure vs Brightness: the exposure is a feature of the sensor, it contols how much light is filtered.
Brightness is an artificial processing done in the isp device. It is just an addition/substruction of the Y channel.
Contrast is done on both the Y and the chromaticy channels.

so exposure is controlled directly by the sensor, and brightness is more of an aritificial isp thing where the Y channel is increased?
Answer: yes that's correct. it can also be decreased. just plain offset positive or negative to the luma component. low exposure can lead to actual limitation in sensor depth effectiveness. what I mean is that if you overexpose, out of 10 or 12 luma bits you will use just 4 or 5. so the actual useful luma information is much reduced, even if you apply negative brightness afterwards

slides of the talk:

https://static.sched.com/hosted_files/osseu2020/eb/hristev-elce2020.pdf
from the slides some other things:

======
6/11/2020

latest valid libcamera commit:
```
root@raspberrypi:~/libcamera# git log -1
commit d5ce2679c67877295ce0096afd3d24d28ad34d16 (HEAD -> master, origin/master, origin/HEAD)
Author: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
Date:   Wed Sep 16 16:04:03 2020 +0300

    libcamera: Turn the android option into a feature

    Allow disabling compilation of the Android HAL adaptation layer
    automatically when a dependency is missing by turning the android option
    into a feature. The default value is set to 'disabled' to match the
    current behaviour.

    Signed-off-by: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
    Reviewed-by: Kieran Bingham <kieran.bingham@ideasonboard.com>
    Reviewed-by: Niklas Söderlund <niklas.soderlund@ragnatech.se>

```
https://www.raspberrypi.org/forums/viewtopic.php?p=1782697

rpi 4 login:
pi:zzzz

https://www.raspberrypi.org/forums/viewtopic.php?p=1782697
https://github.com/raspberrypi/documentation/tree/master/linux/software/libcamera

#### 20.02.2021

bluetooth
---------
The dmesg shows:
```
pi@raspberrypi:~$ dmesg | grep -i blue
[   15.075618] Bluetooth: Core ver 2.22
[   15.075686] Bluetooth: HCI device and connection manager initialized
[   15.075701] Bluetooth: HCI socket layer initialized
[   15.075711] Bluetooth: L2CAP socket layer initialized
[   15.075727] Bluetooth: SCO socket layer initialized
[   15.082082] Bluetooth: HCI UART driver ver 2.3
[   15.082093] Bluetooth: HCI UART protocol H4 registered
[   15.082142] Bluetooth: HCI UART protocol Three-wire (H5) registered
[   15.082288] Bluetooth: HCI UART protocol Broadcom registered
[   15.277417] Bluetooth: BNEP (Ethernet Emulation) ver 1.3
[   15.277431] Bluetooth: BNEP filters: protocol multicast
[   15.277451] Bluetooth: BNEP socket layer initialized
```
the bluetooth is not configurable from the raspi-config

Summary:
--------
I installled (not sure if it was required:
```
sudo apt install bluetooth pi-bluetooth bluez
```
The bottom line to all of this is that in order to see my headphone in the list of bluetooth device
I should first press for 7 seconds on the headphone:
https://helpguide.sony.net/mdr/wh1000xm2/v1/en/contents/TP0001513161.html
After doing that I was able to see the my wh-1000xm2 on the list of devices:
```
pi@raspberrypi:~$ sudo bluetoothctl scan on
Discovery started
....
[NEW] Device 04:5D:4B:EA:C9:C4 WH-1000XM2
```
The running pair and connect:
```
sudo bluetoothctl pair 04:5D:4B:EA:C9:C4
Attempting to pair with 04:5D:4B:EA:C9:C4
[CHG] Device 04:5D:4B:EA:C9:C4 Connected: yes
[CHG] Device 04:5D:4B:EA:C9:C4 Paired: yes
Pairing successful
pi@raspberrypi:~$ sudo bluetoothctl connect 04:5D:4B:EA:C9:C4
Attempting to connect to 04:5D:4B:EA:C9:C4
[CHG] Device 04:5D:4B:EA:C9:C4 Connected: yes
Connection successful
```
Then from the GUI I played an mp3 audio with vlc.
In vlc, I went to 'Audio'->'Audio devices'->'WH-1000XM2'
and it worked!

libcamera with gstreamer
------------------------
to get access to the libcamera element:
```
export GST_PLUGIN_PATH=./build/src/gstreamer
```
streaming:
```
gst-launch-1.0 libcamerasrc ! gdppay ! tcpserversink host=192.168.177.96 port=5000
```
If you get assertion error from include/libcamera/controls.h, then install `doxygen` (`sudo apt install doxygen`) and recompile.

On the linux side:
(might need to install more elements: `sudo apt install gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly`
```
gst-launch-1.0 -v tcpclientsrc host=192.168.177.96 port=5000 ! gdpdepay ! videoconvert ! autovideosink sync=false
```

Note that if you see black , it might be that the camer is with the sensor facing the table hehe.
