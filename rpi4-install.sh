ssh root@10.42.0.228 "if [[ ! -e /boot/kernel7l.img.bak ]]; then cp /boot/kernel7l.img /boot/kernel7l.img.bak; fi"
sudo sshfs -o nonempty root@10.42.0.228:/lib/modules /home/dafna/git/3a-project/sshfs-mount/lib/modules/
sudo make KBUILD_OUTPUT=/home/dafna/git/3a-project/kbuild/rpi4-b-arm7-0 INSTALL_MOD_PATH=/home/dafna/git/3a-project/sshfs-mount modules_install
scp $KBUILD_OUTPUT/arch/arm/boot/zImage root@10.42.0.228:/boot/kernel7l.img
scp $KBUILD_OUTPUT/arch/arm/boot/dts/*.dtb root@10.42.0.228:/boot/
scp $KBUILD_OUTPUT/arch/arm/boot/dts/overlays/*.dtb* root@10.42.0.228:/boot/overlays
scp $KBUILD_OUTPUT/arch/arm/boot/dts/overlays/README root@10.42.0.228:/boot/overlays
scp arch/arm/boot/dts/overlays/README root@10.42.0.228:/boot/overlays

