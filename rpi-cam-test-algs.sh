#!/bin/bash
FRAMES=10
JSON=src/ipa/raspberrypi/data/imx219.json
export LIBCAMERA_LOG_FILE=cam.log

algs=( "black_level"
    "lux"
    "noise"
    "geq"
    "sdn"
    "awb"
    "agc"
    "alsc"
    "contrast"
    "ccm"
    "sharpen"
    "dpc" )

# first, capture with all algorithms on
./build/src/cam/cam -c 1 --capture=$FRAMES --file=all-algs-cam800x600-yuyv#.yuv --stream width=800,height=600,pixelformat=YUYV > /dev/null 2>&1

# disabeling all the algs
sed -i "s/rpi\./rpix./g" $JSON

./build/src/cam/cam -c 1 --capture=$FRAMES --file=no-algs-cam800x600-yuyv#.yuv --stream width=800,height=600,pixelformat=YUYV >/dev/null 2>&1


# for loop that iterates over each element in arr
for i in "${algs[@]}"
do
	echo $i
	sed -i "s/rpix\.$i/rpi.$i/g" $JSON
	grep "rpi\." $JSON
	./build/src/cam/cam -c 1 --capture=$FRAMES --file="algs-$i-cam800x600-yuyv#.yuv" --stream width=800,height=600,pixelformat=YUYV >/dev/null 2>&1
	sed -i "s/rpi\.$i/rpix.$i/g" $JSON
done

sed -i "s/rpix\./rpi./g" $JSON

for f in `ls *$(($FRAMES -  2)).yuv`; do ffmpeg -framerate 30 -video_size 800x600 -pixel_format yuyv422 -i $f $f.mp4 ; done
