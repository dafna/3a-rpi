#!/bin/bash

if (( $# != 1 )); then
    >&2 echo "missing a suffix to the kbuil dir"
	return 1
fi

export KBUILD_OUTPUT=/home/dafna/git/3a-project/kbuild/rpi4-b-arm7-${1}
echo $KBUILD_OUTPUT
export CROSS_COMPILE=arm-linux-gnueabihf-
export ARCH=arm
export KERNEL=kernel7l
export SOURCE=/home/dafna/git/3a-project/rpi-downstream-linux
