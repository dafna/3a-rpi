source ./rpi3.sh

echo $ARCH
echo $O
echo $CROSS_COMPILE
echo $KBUILD_OUTPUT

#make -j9
ROOT=/home/dafna/git/3a-project/mnt/ext4
sudo mount /dev/sdb2 $ROOT || exit 1
cd $KBUILD_OUTPUT  && sudo make INSTALL_MOD_PATH=$ROOT modules_install && cd -

BOOT=/home/dafna/git/3a-project/mnt/fat32
sudo mount /dev/sdb1 $BOOT || exit 1

sudo cp $KBUILD_OUTPUT/arch/$ARCH/boot/zImage $BOOT/$KERNEL.img
# cp $KBUILD_OUTPUT/arch/$ARCH/boot/zImage $BOOT/kernel8.img
sudo cp $KBUILD_OUTPUT/arch/$ARCH/boot/dts/*.dtb $BOOT/
sudo cp $KBUILD_OUTPUT/arch/$ARCH/boot/dts/overlays/*.dtb* $BOOT/overlays
sudo cp $SOURCE/arch/$ARCH/boot/dts/overlays/README $BOOT/overlays

sudo umount $BOOT || exit 1
sudo umount $ROOT || exit 1

echo "Alles gut!"

# umount /media/dafna/rootfs
# umount $BOOT
